package com.example.vasun.magneticdoorlockcontroller;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class KeyboardView extends FrameLayout implements View.OnClickListener {

    private EditText mPasswordField;
    private TextView mMessageField;
    public static final int NO_PASSWORD_INPUTTED = 0;
    public static final int PASSWORD_IS_WRONG = 1;
    public static final int PASSWORD_IS_CORRECT = 2;
    public static final int LOCK_DOOR = 3;
    public static final int DOOR_IS_UNLOCKED = 4;
    public static final int DOOR_IS_LOCKED = 5;

    public KeyboardView(Context context) {
        super(context);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.keyboard, this);
        initViews();
    }

    private void initViews() {
        mPasswordField = $(R.id.password_field);
        mMessageField = $(R.id.message);
        $(R.id.t9_key_0).setOnClickListener(this);
        $(R.id.t9_key_1).setOnClickListener(this);
        $(R.id.t9_key_2).setOnClickListener(this);
        $(R.id.t9_key_3).setOnClickListener(this);
        $(R.id.t9_key_4).setOnClickListener(this);
        $(R.id.t9_key_5).setOnClickListener(this);
        $(R.id.t9_key_6).setOnClickListener(this);
        $(R.id.t9_key_7).setOnClickListener(this);
        $(R.id.t9_key_8).setOnClickListener(this);
        $(R.id.t9_key_9).setOnClickListener(this);
        $(R.id.t9_key_clear).setOnClickListener(this);
        $(R.id.t9_key_backspace).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // handle number button click
        if (v.getTag() != null && "number_button".equals(v.getTag())) {
            mPasswordField.append(((TextView) v).getText());
            return;
        }
        switch (v.getId()) {
            case R.id.t9_key_clear: { // handle clear button
                mPasswordField.setText(null);
            }
            break;
            case R.id.t9_key_backspace: { // handle backspace button
                // delete one character
                Editable editable = mPasswordField.getText();
                int charCount = editable.length();
                if (charCount > 0) {
                    editable.delete(charCount - 1, charCount);
                }
            }
            break;
        }
    }

    public String getInputText() {
        return mPasswordField.getText().toString();
    }

    public void setSignal(int signal) {
        String message = "";

        if (signal == NO_PASSWORD_INPUTTED) {
            message = "PLEASE ENTER PASSWORD";
            setMessage(message, Color.parseColor("#FF8C00"));

        } else if (signal == PASSWORD_IS_WRONG) {
            message = "WRONG PASSWORD";
            setMessage(message, Color.RED);

        } else if (signal == PASSWORD_IS_CORRECT) {
            message = "DOOR UNLOCKED";
            setMessage(message, Color.parseColor("#228B22"));

        } else if (signal == LOCK_DOOR) {
            message = "DOOR LOCKED";
            setMessage(message, Color.BLUE);

        } else if (signal == DOOR_IS_UNLOCKED) {
            message = "DOOR IS ALREADY UNLOCKED";
            setMessage(message, Color.BLACK);

        } else if (signal == DOOR_IS_LOCKED) {
            message = "DOOR IS ALREADY LOCKED";
            setMessage(message, Color.BLACK);
        }

    }

    private void setMessage(String message, int color) {
        mMessageField.setText(message);
        mMessageField.setTextColor(color);
        mPasswordField.setText("");

        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(300); //You can manage the blinking time with this parameter
        anim.setStartOffset(30);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(4);

        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mMessageField.setText("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mMessageField.startAnimation(anim);

    }

    protected <T extends View> T $(@IdRes int id) {
        return (T) super.findViewById(id);
    }
}
