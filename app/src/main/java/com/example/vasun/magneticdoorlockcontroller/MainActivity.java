package com.example.vasun.magneticdoorlockcontroller;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

// source: https://stackoverflow.com/questions/21872150/android-custom-numeric-keyboard
public class MainActivity extends AppCompatActivity {

    private KeyboardView keyboardView;
    private TextView doorStatus;
    private TextSwitcher textSwitcher;
    private Button unlockButton, lockButton;
//    private final String PASSWORD = "5381011";
    private String password;
    private boolean doorIsUnlocked;

    private DatabaseReference passRef;
    private DatabaseReference inputRef;
//    private FirebaseAuth firebaseAuth;

    private String device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        Animation textAnimationIn =  AnimationUtils.
                loadAnimation(this,   android.R.anim.fade_in);
        textAnimationIn.setDuration(800);

        Animation textAnimationOut =  AnimationUtils.
                loadAnimation(this,   android.R.anim.fade_out);
        textAnimationOut.setDuration(800);

        textSwitcher.setInAnimation(textAnimationIn);
        textSwitcher.setOutAnimation(textAnimationOut);
    }

    @Override
    protected void onResume() {
        getPassword();
        super.onResume();
    }

    public void init() {
        findViewById();

        device = getIntent().getStringExtra("device");
        this.setTitle("Device: " + device);

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        passRef = firebaseDatabase.getReference().child(device).child("password");
        passRef.keepSynced(true);
        inputRef = firebaseDatabase.getReference().child(device).child("input");
        inputRef.keepSynced(true);

        getPassword();
        getDoorState();

        setUnlockButton();
        setLockButton();
    }

    public void getPassword() {
        passRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String temp = dataSnapshot.getValue(String.class);
                setPassword(temp);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setPassword(String temp) {
        password = temp;

        Log.d("PASSWORD", password);
    }

    public void setUnlockButton() {

        getDoorState();

        unlockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = keyboardView.getInputText();

                if (doorIsUnlocked) {
                    keyboardView.setSignal(KeyboardView.DOOR_IS_UNLOCKED);
//                    inputRef.setValue(true);
//                    getDoorState();
                } else {
                    if (input.equals("")) {
                        keyboardView.setSignal(KeyboardView.NO_PASSWORD_INPUTTED);
                        inputRef.setValue(false);
                    } else {
                        if (input.equals(password)) {
                            keyboardView.setSignal(KeyboardView.PASSWORD_IS_CORRECT);
                            inputRef.setValue(true);
                            textSwitcher.setText("DOOR UNLOCKED");
                        } else {
                            keyboardView.setSignal(KeyboardView.PASSWORD_IS_WRONG);
                            inputRef.setValue(false);
                        }
                    }
//                    getDoorState();
                }
                getDoorState();
            }
        });
    }

    public void setLockButton() {
//        getDoorState();
        lockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(doorIsUnlocked) {
                    keyboardView.setSignal(KeyboardView.LOCK_DOOR);
                    inputRef.setValue(false);
                    textSwitcher.setText("DOOR LOCKED");
                } else {
                    keyboardView.setSignal(KeyboardView.DOOR_IS_LOCKED);
                }
                getDoorState();
            }
        });
    }

    private void getDoorState() {
        inputRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean flag = dataSnapshot.getValue(Boolean.class);
                setDoorState(flag);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setDoorState(boolean flag) {
        doorIsUnlocked = flag;
    }

    public void findViewById() {
        keyboardView = findViewById(R.id.keypad);
//        doorStatus = findViewById(R.id.door_status);
        textSwitcher = findViewById(R.id.text_switcher);
        unlockButton = findViewById(R.id.unlock_button);
        lockButton = findViewById(R.id.lock_button);
    }
}
