package com.example.vasun.magneticdoorlockcontroller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner spinner;
    private Button check_connection;
    private Button open_device;
    private ProgressBar progress_bar;

    private List<String> devices;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference dbRef;

    private String device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init();
    }

    @Override
    protected void onResume() {
        open_device.setEnabled(false);
        super.onResume();
    }

    public void init() {
        findViewById();

        spinner.setOnItemSelectedListener(this);

        firebaseDatabase = FirebaseDatabase.getInstance();
        dbRef = firebaseDatabase.getReference();
        dbRef.keepSynced(true);

        devices = new ArrayList<>();
        getDevices();

        progress_bar.setVisibility(View.GONE);

        setCheckConnectionButton();
        setOpenDeviceButton();
    }

    private void findViewById() {
        spinner = findViewById(R.id.spinner);
        check_connection = findViewById(R.id.check_connection);
        open_device = findViewById(R.id.open_device);
        progress_bar = findViewById(R.id.progress_bar);
    }

    private void getDevices() {
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    String temp = ds.getKey();
                    devices.add(temp);
                }
                setSpinner();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setSpinner() {
        for (String a : devices) {
            Log.d("KEY", a);
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, devices);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    private void setCheckConnectionButton() {
        check_connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress_bar.setVisibility(View.VISIBLE);
                DatabaseReference deviceRef = firebaseDatabase.getReference().child(device).child("connection");
                getDeviceStatus(deviceRef);
            }
        });
    }

    private void getDeviceStatus(DatabaseReference ref) {
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean status = dataSnapshot.getValue(Boolean.class);
                if (status) {
                    open_device.setEnabled(true);
                    progress_bar.setVisibility(View.GONE);
                    Toast.makeText(HomeActivity.this,
                            "Device is connected!",
                            Toast.LENGTH_SHORT).show();
                } else {
                    open_device.setEnabled(false);
                    progress_bar.setVisibility(View.GONE);
                    Toast.makeText(HomeActivity.this,
                            "Device is not connected...",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setOpenDeviceButton() {
        open_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("device", device);
                Log.d("DEVICE", device);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        device = adapterView.getItemAtPosition(i).toString();
        open_device.setEnabled(false);
        // Showing selected spinner device
        Toast.makeText(adapterView.getContext(), "Check Device Connection!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
